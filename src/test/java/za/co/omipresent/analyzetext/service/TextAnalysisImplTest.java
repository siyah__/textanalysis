package za.co.omipresent.analyzetext.service;

import com.google.cloud.language.v1.*;
import com.google.cloud.language.v1.Document.Type;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import za.co.omipresent.analyzetext.models.SentimentResponse;
import za.co.omipresent.analyzetext.models.Tweets;
import za.co.omipresent.analyzetext.models.TweetsWithAnalysis;
import za.co.omipresent.analyzetext.repository.TweetsRepository;
import za.co.omipresent.analyzetext.repository.TweetsWithAnalysisRepository;
import za.co.omipresent.analyzetext.utils.LanguageServiceClientWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class TextAnalysisImplTest {

    private TextAnalysisServiceImpl textAnalysisService;
    private TweetsWithAnalysisRepository tweetsWithAnalysisRepository;
    private TweetsRepository tweetsRepository;
    private LanguageServiceClientWrapper languageServiceClientWrapper;
    private LanguageServiceClient languageServiceClient;

    @Before
    public void setUp(){
        textAnalysisService = new TextAnalysisServiceImpl();
        tweetsRepository = Mockito.mock(TweetsRepository.class);
        tweetsWithAnalysisRepository = Mockito.mock(TweetsWithAnalysisRepository.class);
        languageServiceClient = Mockito.mock(LanguageServiceClient.class);
        languageServiceClientWrapper = Mockito.mock(LanguageServiceClientWrapper.class);
        textAnalysisService.setLanguageServiceClientWrapper(languageServiceClientWrapper);
        textAnalysisService.setTweetsRepository(tweetsRepository);
        textAnalysisService.setTweetsWithAnalysisRepository(tweetsWithAnalysisRepository);
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    @DisplayName("Given a document, when analyzeSentiment method is called, should return a sentimentResponse.")
    public void shouldAnalyzeSentiment() throws IOException{
        Mockito.when(languageServiceClientWrapper.open()).thenReturn(languageServiceClient);
        Mockito.when(languageServiceClient.analyzeSentiment(ArgumentMatchers.any(Document.class))).thenReturn(returnSentimentResponse());
        SentimentResponse response = textAnalysisService.analyzeSentiment("Hello world. How are you Today?");
        float expScore = 0.2546f;
        float expMagnitude = 0.2546f;
        Assert.assertEquals(expScore, response.getScore(), 0);
        Assert.assertEquals(expMagnitude, response.getMagnitude(), 0);
    }

    @Test
    @DisplayName("Should fetch tweets from repository, then analyze each one without throwing exception")
    public void shouldAnalyzeAndStoreData() throws IOException{
        Mockito.when(tweetsRepository.findAll()).thenReturn(returnListOfTweets());
        Mockito.when(languageServiceClientWrapper.open()).thenReturn(languageServiceClient);
        Mockito.when(languageServiceClient.analyzeSentiment(ArgumentMatchers.any(Document.class))).thenReturn(returnSentimentResponse());
        Mockito.when(tweetsWithAnalysisRepository.saveAll(ArgumentMatchers.anyList())).thenReturn(returnListOfTweetsWithAnalysis());

        List<TweetsWithAnalysis> analysisList = textAnalysisService.analyzeAndStoreData();
        Assert.assertFalse(analysisList.isEmpty());
    }

    @Test
    @DisplayName("Given an empty list from the repository, should throw Internal error")
    public void shouldThrowInternalError() throws IOException{
        exception.expect(InternalError.class);
        exception.expectMessage("The Repository is empty");
        Mockito.when(tweetsRepository.findAll()).thenReturn(new ArrayList<>());

        textAnalysisService.analyzeAndStoreData();
    }



    private List<TweetsWithAnalysis> returnListOfTweetsWithAnalysis(){
        List<TweetsWithAnalysis> analysisList = new ArrayList<>();
        TweetsWithAnalysis tweetsWithAnalysis1 = new TweetsWithAnalysis("girls",
                "i love girls, they are great",
                0.5,
                "Gauteng",
                "Johannesburg",
                "South Africa");
        analysisList.add(tweetsWithAnalysis1);
        TweetsWithAnalysis tweetsWithAnalysis2 = new TweetsWithAnalysis("racism",
                "racism is stupid",
                0.2,
                "Western Cape",
                "Cape Town",
                "South Africa");
        analysisList.add(tweetsWithAnalysis2);

        return analysisList;
    }
    private List<Tweets> returnListOfTweets(){
        List<Tweets> tweetsList = new ArrayList<>();
        Tweets tweet1 = new Tweets("cats", "cats are adorable animals", "Gauteng", "Pretoria");
        tweetsList.add(tweet1);
        Tweets tweet2 = new Tweets("cars", "man I love cars, they are great", "Eastern Cape", "East London");
        tweetsList.add(tweet2);

        return tweetsList;
    }

    private Document returnDocument(){
        return Document.newBuilder()
                .setContent("Hello world. How are you Today?")
                .setType(Type.PLAIN_TEXT)
                .build();
    }

    private AnalyzeSentimentResponse returnSentimentResponse(){
        Sentiment sentiment = Sentiment.newBuilder()
                .setMagnitude(0.2546f)
                .setScore(0.2546f)
                .build();
        return AnalyzeSentimentResponse.newBuilder().setDocumentSentiment(sentiment).build();
    }
}
