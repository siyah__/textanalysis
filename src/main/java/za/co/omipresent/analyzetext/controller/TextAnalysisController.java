package za.co.omipresent.analyzetext.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.omipresent.analyzetext.models.TweetsWithAnalysis;
import za.co.omipresent.analyzetext.service.TextAnalysisService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/analyze")
public class TextAnalysisController {

    private TextAnalysisService textAnalysisService;

    @GetMapping(value = "/sentiment")
    public @ResponseBody List<TweetsWithAnalysis> analyzeSentiment() throws IOException{
        return textAnalysisService.analyzeAndStoreData();
    }

    @Autowired
    public void setTextAnalysisService(TextAnalysisService textAnalysisService){
        this.textAnalysisService = textAnalysisService;
    }

}
