package za.co.omipresent.analyzetext.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omipresent.analyzetext.models.Tweets;

public interface TweetsRepository extends MongoRepository<Tweets, String> {

}
