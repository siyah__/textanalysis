package za.co.omipresent.analyzetext.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omipresent.analyzetext.models.TweetsWithAnalysis;

public interface TweetsWithAnalysisRepository extends MongoRepository<TweetsWithAnalysis, String> {

}
