package za.co.omipresent.analyzetext.utils;

import com.google.cloud.language.v1.LanguageServiceClient;

import java.io.IOException;

public class LanguageServiceClientWrapper {

    public LanguageServiceClient open() throws IOException {
        return LanguageServiceClient.create();
    }

    public void closeClient(LanguageServiceClient languageServiceClient){
        languageServiceClient.close();
    }
}
