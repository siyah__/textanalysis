package za.co.omipresent.analyzetext.service;

import com.google.cloud.language.v1.*;
import com.google.cloud.language.v1.Document.Type;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.omipresent.analyzetext.utils.LanguageServiceClientWrapper;
import za.co.omipresent.analyzetext.models.*;
import za.co.omipresent.analyzetext.repository.TweetsWithAnalysisRepository;
import za.co.omipresent.analyzetext.repository.TweetsRepository;

import java.io.IOException;
import java.util.*;

@Log4j2
public class TextAnalysisServiceImpl implements TextAnalysisService{

    private TweetsRepository tweetsRepository;
    private TweetsWithAnalysisRepository tweetsWithAnalysisRepository;
    private LanguageServiceClientWrapper languageServiceClientWrapper;

    @Override
    public List<TweetsWithAnalysis> analyzeAndStoreData() throws IOException
    {
        SentimentResponse sentimentResponse;
        List<Tweets> tweetsList = tweetsRepository.findAll();
        List<TweetsWithAnalysis> tweetsWithAnalysisList = new ArrayList<>();
        if (tweetsList.isEmpty())
        {
            throw new InternalError("The Repository is empty");
        }

        for (Tweets tweets : tweetsList)
        {
            sentimentResponse = analyzeSentiment(tweets.getFullText());
            tweetsWithAnalysisList.add(new TweetsWithAnalysis(tweets.getTopic(),
                    tweets.getFullText(),
                    sentimentResponse.getScore(),
                    tweets.getProvince(),
                    tweets.getCityName(),
                    "South Africa"));
        }
        tweetsRepository.deleteAll();
        log.info("Tweets have been analyzed and stored in database.");

        return tweetsWithAnalysisRepository.saveAll(tweetsWithAnalysisList);
    }

    @Override
    public SentimentResponse analyzeSentiment(String text) throws IOException
    {
        LanguageServiceClient languageServiceClient = languageServiceClientWrapper.open();
        Document doc = Document.newBuilder()
                .setContent(text)
                .setType(Type.PLAIN_TEXT)
                .build();
        AnalyzeSentimentResponse response = languageServiceClient.analyzeSentiment(doc);
        Sentiment sentiment = response.getDocumentSentiment();
        languageServiceClientWrapper.closeClient(languageServiceClient);
        return new SentimentResponse(sentiment.getMagnitude(), sentiment.getScore());
    }

   @Autowired
   public void setTweetsRepository(TweetsRepository tweetsRepository){
        this.tweetsRepository = tweetsRepository;
   }

    @Autowired
    public void setTweetsWithAnalysisRepository(TweetsWithAnalysisRepository tweetsWithAnalysisRepository){
        this.tweetsWithAnalysisRepository = tweetsWithAnalysisRepository;
    }

    @Autowired
    public void setLanguageServiceClientWrapper(LanguageServiceClientWrapper languageServiceClientWrapper){
        this.languageServiceClientWrapper = languageServiceClientWrapper;
    }

}

