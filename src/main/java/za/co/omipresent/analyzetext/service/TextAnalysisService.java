package za.co.omipresent.analyzetext.service;

import za.co.omipresent.analyzetext.models.SentimentResponse;
import za.co.omipresent.analyzetext.models.TweetsWithAnalysis;

import java.io.IOException;
import java.util.List;

public interface TextAnalysisService {
    SentimentResponse analyzeSentiment(String text) throws IOException;
    List<TweetsWithAnalysis> analyzeAndStoreData() throws IOException;
}
