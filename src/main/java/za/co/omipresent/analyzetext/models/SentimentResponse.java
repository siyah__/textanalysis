package za.co.omipresent.analyzetext.models;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.io.Serializable;

@Data
public class SentimentResponse implements Serializable {

    @JsonAlias({"magnitude_"})
    private double magnitude;
    @JsonAlias({"score_"})
    private double score;

    public SentimentResponse() {
    }

    public SentimentResponse(double magnitude, double score)
    {
        this.magnitude = magnitude;
        this.score = score;
    }
}
