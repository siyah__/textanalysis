package za.co.omipresent.analyzetext.exceptions;


public class InternalError extends RuntimeException{
    public InternalError(String s){super(s);}
}
