package za.co.omipresent.analyzetext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalyzeTextApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalyzeTextApplication.class, args);
	}

}
