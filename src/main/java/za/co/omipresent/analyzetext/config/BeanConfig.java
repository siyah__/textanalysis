package za.co.omipresent.analyzetext.config;

import com.google.cloud.language.v1.LanguageServiceClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import za.co.omipresent.analyzetext.service.TextAnalysisService;
import za.co.omipresent.analyzetext.service.TextAnalysisServiceImpl;
import za.co.omipresent.analyzetext.utils.LanguageServiceClientWrapper;

import java.io.IOException;

@Configuration
public class BeanConfig {

    @Bean
    public RestTemplate restTemplate(){return new RestTemplate();
    }
    @Bean
    public TextAnalysisService textAnalysisService(){return new TextAnalysisServiceImpl();
    }
    @Bean
    public LanguageServiceClient languageServiceClient()throws IOException
    {
        return LanguageServiceClient.create();
    }
    @Bean
    public LanguageServiceClientWrapper languageServiceClientWrapper(){
        return new LanguageServiceClientWrapper();
    }
}
